package test.cc.sales.services;

import jakarta.validation.ValidationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import test.cc.sales.dtos.ProductDto;
import test.cc.sales.entities.Product;
import test.cc.sales.entities.SaleProduct;
import test.cc.sales.repositories.IProductRepository;
import test.cc.sales.repositories.ISaleProductRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProductServiceTest {
    @Autowired
    private ProductService productService;
    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private ISaleProductRepository saleProductRepository;

    @Test
    void createProduct() {
        var productInput = new ProductDto(
                "Product Name", 
                new BigDecimal("1.99")
        );
        
        //Act
        var productCreated = productService.create(productInput);
        
        var products = productRepository.findAll();
        var persistedProduct = products.get(0);
        
        assertProductDtoEqualsProduct(new ProductDto(1, productInput.getName(), productInput.getPrice()), persistedProduct);
        assertEquals(products.size(), 1);
        assertInstanceOf(ProductDto.class, productCreated);
    }

    @Test
    void createProductWithInvalidInput() {
        //Act
        assertThrows(ValidationException.class, () -> productService.create(new ProductDto()));
        
        var products = productRepository.findAll();
        assertEquals(products.size(), 0);
    }

    @Test
    void getAllProducts() {
        var product = new Product();
        product.setName("Product");
        product.setPrice(new BigDecimal("2.99"));
        productRepository.save(product);
        
        //Act
        var productsDtos = productService.getAll();
        var productDto = productsDtos.get(0);
        
        assertInstanceOf(ArrayList.class, productsDtos);
        assertInstanceOf(ProductDto.class, productDto);
        assertEquals(productsDtos.size(), 1);
        assertProductDtoEqualsProduct(productDto, productRepository.findById(1).get());
    }

    @Test
    void getOneExistingProduct() {
        var product = new Product();
        product.setName("Product");
        product.setPrice(new BigDecimal("2.99"));
        productRepository.save(product);

        //Act
        var optionalProductDto = productService.get(1);

        assertInstanceOf(Optional.class, optionalProductDto);
        assertTrue(optionalProductDto.isPresent());
        var productDto = optionalProductDto.get();
        assertInstanceOf(ProductDto.class, productDto);
        assertProductDtoEqualsProduct(productDto, productRepository.findById(1).get());
    }
    
    @Test
    void getOneNonExistingProduct() {
        //Act
        var optionalProductDto = productService.get(1);

        assertTrue(optionalProductDto.isEmpty());
    }

    @Test
    void updateExistingProduct() {
        var existingProduct = new Product();
        existingProduct.setName("Existing Product");
        existingProduct.setPrice(new BigDecimal("5.99"));
        productRepository.save(existingProduct);
        var updatedProductDto = new ProductDto("Updated Product", new BigDecimal("9.99"));

        // Act
        var updatedProductOptional = productService.update(existingProduct.getId(), updatedProductDto);
        
        assertTrue(updatedProductOptional.isPresent());
        var updatedProduct = productRepository.findById(existingProduct.getId()).orElse(null);
        assertNotNull(updatedProduct);
        assertProductDtoEqualsProduct(updatedProductDto, updatedProduct);
    }

    @Test
    void updateNonExistingProduct() {
        var updatedProductDto = new ProductDto("Updated Product", new BigDecimal("9.99"));

        // Act
        assertThrows(RuntimeException.class, () -> productService.update(1, updatedProductDto));
    }

    @Test
    void deleteExistingProductWithoutSale() {
        var existingProduct = new Product();
        existingProduct.setName("Existing Product");
        existingProduct.setPrice(new BigDecimal("5.99"));
        productRepository.save(existingProduct);

        // Act
        var result = productService.delete(existingProduct.getId());
        
        assertEquals("Product deleted successfully", result);
        assertTrue(productRepository.findById(existingProduct.getId()).isEmpty());
    }

    @Test
    void deleteExistingProductWithSale() {
        var existingProduct = new Product();
        existingProduct.setName("Existing Product");
        existingProduct.setPrice(new BigDecimal("5.99"));
        productRepository.save(existingProduct);

        var saleProduct = new SaleProduct();
        saleProduct.setProduct(existingProduct);
        saleProductRepository.save(saleProduct);
        
        //Act
        assertThrows(RuntimeException.class, () -> productService.delete(existingProduct.getId()));
        assertTrue(productRepository.findById(existingProduct.getId()).isPresent()); 
    }
    
    @Test
    void deleteNonExistingProduct() {
        //Act
        var result = productService.delete(1);

        assertEquals("Product not found.", result);
    }

    private void assertProductDtoEqualsProduct(ProductDto expected, Product actual){
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getPrice(), actual.getPrice());
    }
}
