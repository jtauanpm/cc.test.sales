package test.cc.sales.services;

import jakarta.validation.ValidationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import test.cc.sales.dtos.CustomerDto;
import test.cc.sales.entities.Customer;
import test.cc.sales.entities.Sale;
import test.cc.sales.repositories.ICustomerRepository;
import test.cc.sales.repositories.ISaleRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CustomerServiceTest {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ICustomerRepository customerRepository;
    @Autowired
    private ISaleRepository saleRepository;

    @Test
    void createCustomer() {
        var customerDto = new CustomerDto("John", "123456789", false);
        
        // Act
        customerDto = customerService.create(customerDto);
        
        var customer = customerRepository.findById(1);
        assertTrue(customer.isPresent());
        assertCustomerDtoEqualsCustomer(customerDto, customer.get());
    }

    @Test
    void createCustomerWithExistingPhoneNumber() {
        var existingCustomer = customerFactory( "John", "987654321", true);
        customerRepository.save(existingCustomer);

        // Act
        assertThrows(ValidationException.class, () -> customerService.create(new CustomerDto("Paul", existingCustomer.getPhoneNumber(), false)));
    }

    @Test
    void getAllCustomers() {
        var customers = List.of(
                customerFactory("John", "111111111", true),
                customerFactory("Paul", "222222222", true)
        );
        customerRepository.saveAll(customers);
        
        // Act
        var customerDtos = customerService.getAll();
        
        assertInstanceOf(List.class, customerDtos);
        assertInstanceOf(CustomerDto.class, customerDtos.get(1));
        assertEquals(customerDtos.size(), customers.size());
    }
    
    @Test
    void getExistingCustomer() {
        var existingCustomer = customerFactory("Existing Customer", "333333333", true);
        customerRepository.save(existingCustomer);

        // Act
        var optionalCustomerDto = customerService.get(existingCustomer.getId());

        // Assert
        assertTrue(optionalCustomerDto.isPresent());
        assertInstanceOf(Optional.class, optionalCustomerDto);
        assertCustomerDtoEqualsCustomer(optionalCustomerDto.get(), existingCustomer);
    }

    @Test
    void getNonExistingCustomer() {
        // Act
        var optionalCustomerDto = customerService.get(1);
        
        assertTrue(optionalCustomerDto.isEmpty());
    }

    @Test
    void updateCustomer() {
        var customer = customerFactory("John", "121231213", true);
        customerRepository.save(customer);
        var customerDto = new CustomerDto("Paul", "898989899", false);
        
        // Act
        var optionalCustomerDto = customerService.update(customer.getId(), customerDto);
        
        var customerUpdated = customerRepository.findById(customer.getId());
        assertTrue(optionalCustomerDto.isPresent());
        assertTrue(customerUpdated.isPresent()); 
        assertCustomerDtoEqualsCustomer(customerDto, customerUpdated.get());
    }

    @Test
    void updateNonExistingCustomer() {
        // Act
        var optionalCustomerDto = customerService.update(1, new CustomerDto("Paul", "898989899", false));
        
        assertTrue(optionalCustomerDto.isEmpty());
    }

    @Test
    void updateCustomerWithAlreadyUsedPhoneNumber() {
        var existingCustomer = customerFactory("John", "121231213", true);
        customerRepository.save(existingCustomer);
        
        // Act
        assertThrows(ValidationException.class, () ->
                customerService.update(existingCustomer.getId(), new CustomerDto("Paul", existingCustomer.getPhoneNumber(), false)));
    }

    @Test
    void deleteAnExistingCustomer() {
        var existingCustomer = customerFactory("John", "121231213", true);
        customerRepository.save(existingCustomer);

        // Act
        var result = customerService.delete(existingCustomer.getId());
        
        var optionalCustomer = customerRepository.findById(existingCustomer.getId());
        assertTrue(optionalCustomer.isEmpty());
        assertEquals("Customer deleted successfully", result);
    }

    @Test
    void deleteAnExistingCustomerWithSale() {
        var existingCustomer = customerFactory("John", "121231213", true);
        customerRepository.save(existingCustomer);
        var sale = new Sale();
        sale.setCustomer(existingCustomer);
        saleRepository.save(sale);

        // Act
        assertThrows(ValidationException.class, () -> customerService.delete(existingCustomer.getId()));
    }

    private void assertCustomerDtoEqualsCustomer(CustomerDto expected, Customer actual){
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getPhoneNumber(), actual.getPhoneNumber());
        assertEquals(expected.getStatus(), actual.getStatus());
    }
    
    private Customer customerFactory(String name, String phoneNumber, boolean status){
        var customer = new Customer();
        customer.setName(name);
        customer.setPhoneNumber(phoneNumber);
        customer.setStatus(status);
        return customer;
    }
}
