package test.cc.sales.services;

import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import test.cc.sales.dtos.SaleDto;
import test.cc.sales.dtos.SaleProductDto;
import test.cc.sales.entities.Customer;
import test.cc.sales.entities.Product;
import test.cc.sales.entities.Sale;
import test.cc.sales.entities.SaleProduct;
import test.cc.sales.repositories.ICustomerRepository;
import test.cc.sales.repositories.IProductRepository;
import test.cc.sales.repositories.ISaleProductRepository;
import test.cc.sales.repositories.ISaleRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SaleServiceTest {
    
    @Autowired
    private SaleService saleService;

    @Autowired
    public ICustomerRepository customerRepository;
    @Autowired
    public IProductRepository productRepository;
    @Autowired
    public ISaleRepository saleRepository;
    @Autowired
    public ISaleProductRepository saleProductRepository;

    @Test
    void createSale() {
        var customer = createAndSaveCustomer();
        var product = createAndSaveProduct();
        var saleProductDto = new SaleProductDto(product.getId(), 2);
        var saleDto = new SaleDto(customer.getId(), List.of(saleProductDto));

        // Act
        saleService.create(saleDto);
        
        var persistedSale = saleRepository.findById(1);
        assertTrue(persistedSale.isPresent());
        AssertSaleDtoEqualsSale(saleDto, persistedSale.get());
    }

    @Test
    void createSaleWithInvalidProduct() {
        var customer = createAndSaveCustomer();
        var saleProductDto = new SaleProductDto(1, 2);
        var saleDto = new SaleDto(customer.getId(), List.of(saleProductDto));

        // Act
        assertThrows(ValidationException.class, () -> saleService.create(saleDto));
    }

    @Test
    void createSaleWithInvalidQuantity() {
        var customer = createAndSaveCustomer();
        var product = createAndSaveProduct();
        var saleProductDto = new SaleProductDto(product.getId(), 0);
        var saleDto = new SaleDto(customer.getId(), List.of(saleProductDto));

        // Act
        assertThrows(ValidationException.class, () -> saleService.create(saleDto));
    }

    @Test
    void getAllSales() {
        var sale1 = createAndSaveSale(List.of(createAndSaveProduct(), createAndSaveProduct()));
        var sale2 = createAndSaveSale(List.of(createAndSaveProduct()));

        // Act
        var allSales = saleService.getAll();
        
        assertFalse(allSales.isEmpty());
        assertEquals(allSales.size(), 2);
        
        AssertSaleDtoEqualsSale(allSales.get(0), sale1);
        AssertSaleDtoEqualsSale(allSales.get(1), sale2);
    }

    @Test
    void getExistingSale() {
        var sale = createAndSaveSale(List.of(createAndSaveProduct(), createAndSaveProduct()));

        // Act
        var optionalSaleDto = saleService.get(sale.getId());
        
        assertTrue(optionalSaleDto.isPresent());
        AssertSaleDtoEqualsSale(optionalSaleDto.get(), sale);
    }

    @Test
    void getNonExistingSale() {
        // Act
        var optionalSaleDto = saleService.get(1);
        
        assertTrue(optionalSaleDto.isEmpty());
    }

    @Test
    void deleteExistingSale() {
        var sale = createAndSaveSale(List.of(createAndSaveProduct()));

        // Act
        var result = saleService.delete(sale.getId());
        
        assertEquals("Sale deleted successfully", result);
        assertTrue(saleRepository.findById(sale.getId()).isEmpty());
    }

    @Test
    void deleteNonExistingSale() {
        // Act
        String result = saleService.delete(1);
        
        assertEquals("Sale not found", result);
    }

    @Test
    void deleteExistingUniqueSaleProduct() {
        var saleProductToDelete = createAndSaveProduct();
        var sale = createAndSaveSale(List.of(saleProductToDelete));
        
        // Act
        var result = saleService.deleteSaleProduct(saleProductToDelete.getId());
        
        assertEquals("Sale product deleted successfully", result);
        assertTrue(saleProductRepository.findById(saleProductToDelete.getId()).isEmpty());
        assertTrue(saleRepository.findById(sale.getId()).isEmpty());
    }

    @Test
    void deleteNonExistingSaleProduct() {
        // Act
        String result = saleService.deleteSaleProduct(1);
        
        assertEquals("Sale product not found", result);
    }

    @Test
    void updateExistingSale() {
        var product = createAndSaveProduct();
        var sale = createAndSaveSale(List.of(product));
        
        var saleDto = new SaleDto(sale);
        saleDto.setSaleProducts(List.of(new SaleProductDto(product.getId(), 2, new BigDecimal("99"))));
        
        // Act
        var result = saleService.update(sale.getId(), saleDto);

        var saleUpdated = saleRepository.findById(sale.getId());
        assertTrue(result.isPresent());
        AssertSaleDtoEqualsSale(saleDto, saleUpdated.get());
    } 
    
    @Test
    void updateExistingSaleWithInvalidQuantity() {
        var product = createAndSaveProduct();
        var sale = createAndSaveSale(List.of(product));

        var saleDto = new SaleDto(sale);
        saleDto.setSaleProducts(List.of(new SaleProductDto(product.getId(), 0, new BigDecimal("99"))));

        // Act
        assertThrows(ValidationException.class, () -> saleService.update(sale.getId(), saleDto));
    }

    @Test
    void updateExistingSaleWithInvalidPrice() {
        var product = createAndSaveProduct();
        var sale = createAndSaveSale(List.of(product));

        var saleDto = new SaleDto(sale);
        saleDto.setSaleProducts(List.of(new SaleProductDto(product.getId(), 1, new BigDecimal("0"))));

        // Act
        assertThrows(ValidationException.class, () -> saleService.update(sale.getId(), saleDto));
    }
    
    @Test
    void updateNonExistingSale() {
        var saleDto = new SaleDto();
        
        // Act
        var result = saleService.update(1, saleDto);
        
        assertTrue(result.isEmpty());
    }
    
    private Sale saleFactory(Customer customer, List<Product> products){
        var sale = new Sale();
        var saleProducts = new ArrayList<SaleProduct>();
        
        products.forEach(product -> {
            var saleProduct = saleProductFactory(product,sale);
            saleProducts.add(saleProduct);
        });
        sale.setCustomer(customer);
        sale.setSaleProducts(saleProducts);
        return sale;
    }
    
    private SaleProduct saleProductFactory(Product product, Sale sale){
        var saleProduct = new SaleProduct();
        saleProduct.setProduct(product);
        saleProduct.setQuantity(2);
        saleProduct.setPrice(product.getPrice());
        saleProduct.setSale(sale);
        
        return saleProduct;
    }

    private Customer createAndSaveCustomer() {
        //TODO: generate random customer data
        var customer = new Customer();
        customer.setName("John");
        customerRepository.save(customer);
        return customer;
    }

    private Product createAndSaveProduct() {
        //TODO: generate random product data
        var product = new Product();
        product.setName("Product");
        product.setPrice(new BigDecimal("9.99"));
        productRepository.save(product);
        return product;
    }
    
    private Sale createAndSaveSale(List<Product> products){
        var sale = saleFactory(createAndSaveCustomer(), products);
        saleRepository.save(sale);
        return sale;
    }
    
    private void AssertSaleDtoEqualsSale(SaleDto expected, Sale actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getCustomerId(), actual.getCustomer().getId());

        for (int i = 0; i < expected.getSaleProducts().size(); i++) {
            var expectedSaleProductDto = expected.getSaleProducts().get(i);
            var actualSaleProduct = actual.getSaleProducts().get(i);

            assertEquals(expectedSaleProductDto.getProductId(), actualSaleProduct.getProduct().getId());
            assertEquals(expectedSaleProductDto.getQuantity(), actualSaleProduct.getQuantity());
            assertEquals(expectedSaleProductDto.getPrice(), actualSaleProduct.getPrice());
        }
    }
}
