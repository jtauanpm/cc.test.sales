package test.cc.sales.controllers;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import test.cc.sales.dtos.CustomerDto;
import test.cc.sales.services.CustomerService;

import java.util.List;

@RestController
@RequestMapping(path = "customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping
    public ResponseEntity<CustomerDto> create(@RequestBody @Valid CustomerDto customerDto){
        var response = customerService.create(customerDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping
    public ResponseEntity<List<CustomerDto>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(customerService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable(value="id")int id){
        var optionalCustomerDto = customerService.get(id);
        return (optionalCustomerDto.isEmpty()) ?
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("Customer not found.") :
                ResponseEntity.status(HttpStatus.OK).body(optionalCustomerDto.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@PathVariable(value="id")int id, @RequestBody @Valid CustomerDto customerDto){
        var response = customerService.update(id, customerDto);

        return (response.isEmpty()) ?
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("Customer not found.") :
                ResponseEntity.status(HttpStatus.OK).body(response.get());
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable(value="id")int id){
        return customerService.delete(id);
    }

    //TODO: activate and deactivate status endpoint
}
