package test.cc.sales.controllers;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import test.cc.sales.dtos.ProductDto;
import test.cc.sales.services.ProductService;

import java.util.List;

@RestController
@RequestMapping(path = "products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseEntity<ProductDto> create(@RequestBody @Valid ProductDto productDto){
        var response = productService.create(productDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping
    public ResponseEntity<List<ProductDto>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(productService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable(value="id")int id){
        var optionalProductDto = productService.get(id);
        return (optionalProductDto.isEmpty()) ?
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found.") :
                ResponseEntity.status(HttpStatus.OK).body(optionalProductDto.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@PathVariable(value="id")int id, @RequestBody @Valid ProductDto productDto){
        var response = productService.update(id, productDto);

        return (response.isEmpty()) ?
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found.") :
                ResponseEntity.status(HttpStatus.OK).body(response.get());
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable(value="id")int id){
        return productService.delete(id);
    }

}
