package test.cc.sales.controllers;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import test.cc.sales.dtos.ProductDto;
import test.cc.sales.dtos.SaleDto;
import test.cc.sales.services.SaleService;

import java.util.List;

@RestController
@RequestMapping(path = "sales")
public class SaleController {
    @Autowired
    private SaleService saleService;
    @PostMapping
    public ResponseEntity<SaleDto> create(@RequestBody SaleDto saleDto){
        var response = saleService.create(saleDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(response.get());
    }

    @GetMapping
    public ResponseEntity<List<SaleDto>> getAll(){
        var response = saleService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable(value="id")int id){
        var response = saleService.get(id);
        return (response.isEmpty()) ?
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("Sale not found.") :
                ResponseEntity.status(HttpStatus.OK).body(response.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@PathVariable(value="id")int id, @RequestBody SaleDto saleDto){
        var response = saleService.update(id, saleDto);

        return (response.isEmpty()) ?
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("Sale not found.") :
                ResponseEntity.status(HttpStatus.OK).body(response.get());
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable(value="id")int id){
        return saleService.delete(id);
    }

    @DeleteMapping("/{saleId}/saleproduct/{saleProductid}")
    public String deleteSaleProduct(@PathVariable(value="saleProductid")int saleProductid){
        return saleService.deleteSaleProduct(saleProductid);
    }
}
