package test.cc.sales.handlers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import test.cc.sales.errors.AppErrorResponse;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class AppExceptionHandler {
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<AppErrorResponse> handleAnyException(Exception e) {
        var errorResponse = new AppErrorResponse();
        Map<String, String> errors = new HashMap<>();
        errorResponse.setTime(new Date());
        errorResponse.setStatus(HttpStatus.BAD_REQUEST);

        if(e instanceof MethodArgumentNotValidException){
            var ex = (MethodArgumentNotValidException) e;

            ex.getBindingResult().getAllErrors().forEach(error -> {
                var fieldName = ((FieldError) error).getField();
                var errorMessage = error.getDefaultMessage();
                errors.put(fieldName, errorMessage);
            });
            errorResponse.setErrorMessage(errors);

            return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }else {
            errors.put("error", e.getLocalizedMessage());
            errorResponse.setErrorMessage(errors);
            return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
}
