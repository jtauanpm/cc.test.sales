package test.cc.sales.services;

import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.cc.sales.dtos.SaleDto;
import test.cc.sales.dtos.SaleProductDto;
import test.cc.sales.entities.Customer;
import test.cc.sales.entities.Product;
import test.cc.sales.entities.Sale;
import test.cc.sales.entities.SaleProduct;
import test.cc.sales.repositories.ICustomerRepository;
import test.cc.sales.repositories.IProductRepository;
import test.cc.sales.repositories.ISaleProductRepository;
import test.cc.sales.repositories.ISaleRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SaleService {
    @Autowired
    private ICustomerRepository customerRepository;
    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private ISaleRepository saleRepository;
    @Autowired
    private ISaleProductRepository saleProductRepository;

    public Optional<SaleDto> create(SaleDto saleDto){
        var customer = getValidCustomer(saleDto);
        var sale = new Sale();
        sale.setCustomer(customer);

        var saleProducts = getSaleProductsFromDtos(sale, saleDto.getSaleProducts(),false);
        sale.setSaleProducts(saleProducts);

        saleRepository.save(sale);

        saleDto.setSum(sale.calculateTotalSum());
        saleDto.setId(sale.getId());
        saleDto.setSaleProducts(getSaleProductsDtoFromSale(sale));
        return Optional.of(saleDto);
    }

    @Transactional
    public Optional<SaleDto> update(int id, SaleDto saleDto){
        var optionalSale = saleRepository.findById(id);
        if(optionalSale.isEmpty()) return Optional.empty();
        var customer = getValidCustomer(saleDto);
        var sale = optionalSale.get();
        sale.setCustomer(customer);
        sale.setSaleProducts(new ArrayList<>());
        sale.setSaleProducts(getSaleProductsFromDtos(sale, saleDto.getSaleProducts(), true));

        saleProductRepository.deleteBySale(sale);
        saleRepository.save(sale);

        saleDto.setSum(sale.calculateTotalSum());
        saleDto.setId(sale.getId());
        saleDto.setSaleProducts(getSaleProductsDtoFromSale(sale));
        return Optional.of(saleDto);
    }

    public List<SaleDto> getAll(){
        return saleRepository.findAll()
                .stream()
                .map(sale -> {
                    var salesProductsDto = new ArrayList<SaleProductDto>();
                    sale.getSaleProducts().forEach(saleProduct -> salesProductsDto.add(new SaleProductDto(
                            saleProduct.getId(), 
                            saleProduct.getProduct().getId(), 
                            saleProduct.getQuantity(), 
                            saleProduct.getPrice())));
                    return new SaleDto(sale.getId(), sale.getCustomer().getId(), salesProductsDto, sale.calculateTotalSum());
                }).collect(Collectors.toList());
    }

    public Optional<SaleDto> get(int id){
        var optionalSale = saleRepository.findById(id);
        if(optionalSale.isEmpty()) return Optional.empty();
        var sale = optionalSale.get();
        var saleDto = new SaleDto(sale);
        saleDto.setSum(sale.calculateTotalSum());
        return Optional.of(saleDto);
    }

    public String delete(int id){
        try{
            var optionalSale = saleRepository.findById(id);
            if(optionalSale.isEmpty()) return "Sale not found";
            saleRepository.delete(optionalSale.get());
            return "Sale deleted successfully";
        }catch (Exception e){
            throw new RuntimeException("Cannot delete this sale");
        }
    }

    public String deleteSaleProduct(int saleProductId){
        try{
            var optionalSaleProduct = saleProductRepository.findById(saleProductId);
            if(optionalSaleProduct.isEmpty()) return "Sale product not found";
            var saleProduct = optionalSaleProduct.get();
            saleProductRepository.delete(saleProduct);
            if(shouldDeleteSale(saleProduct.getSale())) saleRepository.delete(saleProduct.getSale());
            return "Sale product deleted successfully";
        }catch (Exception e){
            throw new RuntimeException("Cannot delete this sale product");
        }
    }
    
    private boolean shouldDeleteSale(Sale sale){
        return sale.getSaleProducts().size() == 1;
    }

    private List<SaleProduct> getSaleProductsFromDtos(Sale sale, List<SaleProductDto> saleProductDtos, boolean isUpdating){
        var saleProducts = new ArrayList<SaleProduct>();

        for (SaleProductDto saleProductDto : saleProductDtos) {
            validateSaleProductDto(saleProductDto, isUpdating);
            var product = getValidateProductById(saleProductDto.getProductId());

            var saleProduct = new SaleProduct();
            saleProduct.setProduct(product);
            saleProduct.setQuantity(saleProductDto.getQuantity());
            saleProduct.setSale(sale);

            //If not updating, the product's current price will be used
            saleProduct.setPrice(isUpdating ? saleProductDto.getPrice() : product.getPrice());

            saleProducts.add(saleProduct);
        }

        return saleProducts;
    }

    private Customer getValidCustomer(SaleDto saleDto){
        var optionalCustomer = customerRepository.findById(saleDto.getCustomerId());
        if(optionalCustomer.isEmpty()) throw new RuntimeException("Customer not found");

        return optionalCustomer.get();
    }

    private List<SaleProductDto> getSaleProductsDtoFromSale(Sale sale) {
        return sale.getSaleProducts().stream()
                .map(SaleProductDto::new)
                .collect(Collectors.toList());
    }

    private void validateSaleProductDto(SaleProductDto input, boolean isUpdating){
        if(input.getQuantity() <= 0 ) {
            throw new ValidationException("Product" + input.getProductId() + ": Invalid quantity");
        }
        if(isUpdating){
            if (input.getPrice() == null || input.getPrice().compareTo(BigDecimal.ZERO) <= 0 )
                throw new ValidationException("Updated product "+ input.getProductId() + ": Price cannot be lower than zero. Provide product prices to continue.");

        }
    }

    private Product getValidateProductById(int id) {
        var optionalProduct = productRepository.findById(id);
        if(optionalProduct.isEmpty())
            throw new ValidationException("Product" + id + ": not found or invalid quantity");
        
        return optionalProduct.get();
    }
}
