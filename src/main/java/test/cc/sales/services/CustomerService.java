package test.cc.sales.services;

import jakarta.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.cc.sales.dtos.CustomerDto;
import test.cc.sales.entities.Customer;
import test.cc.sales.repositories.ICustomerRepository;
import test.cc.sales.repositories.ISaleRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerService {
    @Autowired
    private ICustomerRepository customerRepository;
    @Autowired
    private ISaleRepository saleRepository;

    public CustomerDto create(CustomerDto customerDto){
        if(customerRepository.existsByPhoneNumber(customerDto.getPhoneNumber())){
            throw new ValidationException("Customer phone number already exists");
        }
        var customer = new Customer();
        customer.setName(customerDto.getName());
        customer.setPhoneNumber(customerDto.getPhoneNumber());
        customer.setStatus(customerDto.getStatus());

        customerRepository.save(customer);

        customerDto.setId(customer.getId());
        return customerDto;
    }

    public List<CustomerDto> getAll() {
        return customerRepository.findAll()
                .stream()
                .map(customer ->
                        new CustomerDto(customer.getId(), customer.getName(), customer.getPhoneNumber(), customer.getStatus()))
                .collect(Collectors.toList());
    }

    public Optional<CustomerDto> get(int id){
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if(optionalCustomer.isEmpty()) return Optional.empty();
        var customer = optionalCustomer.get();
        var customerDto = new CustomerDto(customer.getId(), customer.getName(), customer.getPhoneNumber(), customer.getStatus());
        return Optional.of(customerDto);
    }

    public Optional<CustomerDto> update(int id, CustomerDto customerDto){
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if(optionalCustomer.isEmpty()) return Optional.empty();

        if(customerRepository.existsByPhoneNumber(customerDto.getPhoneNumber())){
            throw new ValidationException("Customer phone number already exists");
        }

        var customer = optionalCustomer.get();
        customer.setName(customerDto.getName());
        customer.setPhoneNumber(customerDto.getPhoneNumber());
        customer.setStatus(customerDto.getStatus());

        customerRepository.save(customer);

        customerDto.setId(customer.getId());
        return Optional.of(customerDto);
    }

    public String delete(int id){
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if(optionalCustomer.isEmpty()) return "Customer not found.";

        var customer = optionalCustomer.get();
        if (saleRepository.existsByCustomerId(customer.getId())) throw new ValidationException("It is not possible to delete a customer who has a sale");
        customerRepository.delete(customer);

        return "Customer deleted successfully";
    }
}
