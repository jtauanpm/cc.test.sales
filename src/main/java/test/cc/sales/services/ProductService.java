package test.cc.sales.services;

import jakarta.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import test.cc.sales.dtos.ProductDto;
import test.cc.sales.entities.Product;
import test.cc.sales.repositories.IProductRepository;
import test.cc.sales.repositories.ISaleProductRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private ISaleProductRepository saleProductRepository;

    public ProductDto create(ProductDto productDto){
        var product = new Product();
        validateProductDtoInput(productDto);

        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());

        productRepository.save(product);

        productDto.setId(product.getId());
        return productDto;
    }

    public List<ProductDto> getAll() {
        return productRepository.findAll()
                .stream()
                .map(product ->
                        new ProductDto(product.getId(), product.getName(), product.getPrice()))
                .collect(Collectors.toList());
    }

    public Optional<ProductDto> get(int id){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if(optionalProduct.isEmpty()) return Optional.empty();
        var product = optionalProduct.get();
        var productDto = new ProductDto(product.getId(), product.getName(), product.getPrice());
        return Optional.of(productDto);
    }

    public Optional<ProductDto> update(int id, ProductDto productDto){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if(optionalProduct.isEmpty()) throw new RuntimeException("Product not found");

        validateProductDtoInput(productDto);
        var product = optionalProduct.get();

        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());

        productRepository.save(product);

        productDto.setId(product.getId());
        return Optional.of(productDto);
    }

    public String delete(int id){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if(optionalProduct.isEmpty()) return "Product not found.";

        var existSaleForProduct = saleProductRepository.existsByProductId(optionalProduct.get().getId());
        if(existSaleForProduct) throw new RuntimeException("It is not possible to delete this product as it has already been sold");

        productRepository.delete(optionalProduct.get());

        return "Product deleted successfully";
    }

    private void validateProductDtoInput(ProductDto input){
        if (input.getPrice() == null || input.getPrice().compareTo(BigDecimal.ZERO) <= 0 ) throw new ValidationException("Product value cannot be lower than zero");
        if (!StringUtils.hasText(input.getName())) throw new ValidationException("Product name cannot be null or blank");
    }
}
