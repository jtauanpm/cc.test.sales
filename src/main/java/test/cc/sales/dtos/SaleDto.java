package test.cc.sales.dtos;

import test.cc.sales.entities.Sale;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SaleDto {
    public SaleDto(){}
    public SaleDto(Sale sale){
        List<SaleProductDto> saleProductDtos = new ArrayList<>();
        var saleProductsDto = sale.getSaleProducts();
        saleProductsDto.forEach(saleProduct ->
                saleProductDtos.add(
                        new SaleProductDto(saleProduct.getId(),
                                saleProduct.getProduct().getId(),
                                saleProduct.getQuantity(), 
                                saleProduct.getPrice())));

        this.saleProducts = saleProductDtos;
        this.id = sale.getId();
        this.customerId = sale.getCustomer().getId();
    }

    public SaleDto(int id, int customerId, List<SaleProductDto> saleProducts, BigDecimal sum) {
        this.id = id;
        this.customerId = customerId;
        this.saleProducts = saleProducts;
        this.sum = sum;
    }

    public SaleDto(int customerId, List<SaleProductDto> saleProducts) {
        this.customerId = customerId;
        this.saleProducts = saleProducts;
    }

    private int id;
    private int customerId;
    private List<SaleProductDto> saleProducts;

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    private BigDecimal sum;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public List<SaleProductDto> getSaleProducts() {
        return saleProducts;
    }

    public void setSaleProducts(List<SaleProductDto> saleProducts) {
        this.saleProducts = saleProducts;
    }
}
