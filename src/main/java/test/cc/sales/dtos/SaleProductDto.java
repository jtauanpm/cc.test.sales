package test.cc.sales.dtos;

import test.cc.sales.entities.SaleProduct;

import java.math.BigDecimal;

public class SaleProductDto {
    public SaleProductDto(){}

    public SaleProductDto(int id, int productId, int quantity, BigDecimal price) {
        this.id = id;
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
    }

    public SaleProductDto(int productId, int quantity, BigDecimal price) {
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
    }

    public SaleProductDto(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public SaleProductDto(SaleProduct saleProduct) {
        this.id = saleProduct.getId();
        this.productId = saleProduct.getProduct().getId();
        this.quantity = saleProduct.getQuantity();
        this.price = saleProduct.getPrice();
    }

    private int id;
    private int productId;
    private int quantity;

    private BigDecimal price;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
