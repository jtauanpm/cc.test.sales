package test.cc.sales.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;


public class ProductDto {
    public ProductDto(int id, String name, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
    
    public ProductDto(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public ProductDto() {}

    private int id;
    @NotBlank(message = "Name cannot be blank")
    private String name;
    @NotNull(message = "Value cannot be null")
    private BigDecimal price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
