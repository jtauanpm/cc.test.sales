package test.cc.sales.entities;

import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "Sales")
public class Sale implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "sale", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private List<SaleProduct> saleProducts;

    @Transient
    private BigDecimal sum;

    public BigDecimal calculateTotalSum(){
        var saleProducts = this.getSaleProducts();
        BigDecimal sum = BigDecimal.ZERO;

        for (SaleProduct saleProduct : saleProducts) {
            BigDecimal productValue = saleProduct.getPrice();
            BigDecimal quantity = BigDecimal.valueOf(saleProduct.getQuantity());
            sum = sum.add(productValue.multiply(quantity));
        }
        this.sum = sum;
        return sum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<SaleProduct> getSaleProducts() {
        return saleProducts;
    }

    public void setSaleProducts(List<SaleProduct> saleProducts) {
        this.saleProducts = saleProducts;
    }
}
