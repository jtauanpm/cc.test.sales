package test.cc.sales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import test.cc.sales.entities.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Integer> {
    boolean existsByPhoneNumber(String phoneNumber);
}
