package test.cc.sales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import test.cc.sales.entities.Product;

@Repository
public interface IProductRepository extends JpaRepository<Product, Integer> {
}
