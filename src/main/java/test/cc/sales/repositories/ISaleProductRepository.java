package test.cc.sales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import test.cc.sales.entities.Sale;
import test.cc.sales.entities.SaleProduct;

@Repository
public interface ISaleProductRepository extends JpaRepository<SaleProduct, Integer> {
    boolean existsByProductId(int productId);
    void deleteBySale(Sale sale);
}
