package test.cc.sales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import test.cc.sales.entities.Sale;

@Repository
public interface ISaleRepository extends JpaRepository<Sale, Integer> {
    boolean existsByCustomerId(int customerId);
}
