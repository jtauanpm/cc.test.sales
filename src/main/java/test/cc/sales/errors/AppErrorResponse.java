package test.cc.sales.errors;

import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.Map;

public class AppErrorResponse {
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Map<String, String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Map<String, String> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    private Date time;
    private Map<String, String> errorMessage;
    private HttpStatus status;
}
